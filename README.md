# Parallel Hbonds

This is a small repository to provide a parallelized implementation of hydrogen bond calculations as implemented in the [HBonds plugin in VMD](https://www.ks.uiuc.edu/Research/vmd/plugins/hbonds/). It consists of the `hbonds.py` program, which uses pythons multiprocessing in combination with prody and numpy to calculate the hydrogen bonds formed by all atoms in the protein. These are then written to a file containing one line per frame with detailed information on the atoms forming hydrogen bonds in the protein. 

## Installation

### Using pip

```
pip install -r requirements.txt
```

### Using conda

```
conda env create -f environment.yml
```

## Usage: 

```
python hbonds.py [-h] [--cutoff CUTOFF] [--angle ANGLE] [--maxFrame MAXFRAME] [--chunk_size CHUNK_SIZE] [--selection SELECTION] [--num_cpu NUM_CPU]
                 pdb psf out_folder dcd [dcd ...]
```
Use `python hbonds.py -h` for more details on the individual parameters.

## Output

The output file contains one line per frame of the simulation. The file is tab delimited, with every entry being of the form `<resid donor>:<atom donor>-<resid acceptor>:<atom acceptor>`. 

## Analysis of the output file

The repository comes with a script to parse the output file and extract some information from it. `hbonds_parsing.py` contains a `parse(file)` function, which can parse a multiple `hbonds.py` output files and functions `collapse` and `get_interactions` offering some options for analysis. `collapse` will count the number of hydrogen bonds per residue per frame and yields a number of residues by number of frames numpy array.
`get_interactions` will calculate for every residue, with which other residues these interact, allowing for a detailed analysis of the interactions of certain residues.

One can also use the `hbonds_parsing.py` script as an executable, calling the `collapse` and `get_interactions` on a certain subset of residues and with the collapse output further reduced yielding either a all hydrogen bonds per frame or a all hydrogen bonds per residue output. 
One needs to be carefull on the output here, as collapse will count every hydrogen bond twice for both the donor and the acceptor.

## Future plans

In theory the mode of parallelization applied here could be used on many prody (or other simulation analysis tools) functions. In future we would like to implement this parallelization for additional functions and provide a "parallel prody".

This could also be expanded to utilize multiple nodes using MPI. With parallelization over the frames this should easily be possible.

## Citation

Please cite as:

Manthey, Georg, Frederiksen, Anders & Solov'yov, Ilia A. (2023). Parallel Hbonds. Zenodo. https://doi.org/10.5281/zenodo.8051049