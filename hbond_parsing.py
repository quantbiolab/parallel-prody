import numpy as np
import yaml

def parse(file : str) -> 'tuple[list[dict[int,tuple[int,str,str]]],np.ndarray[int]]':
    '''Parse a hbonds.py output file
    '''
    hbonds : "list[dict[int,tuple[int,str,str]]]" = []
    residues : "set[int]" = set()
    with open(file, 'r') as in_file:
        for line in in_file:
            line = ':'.join(line.strip().split(':')[1:])
            data : "dict[int,list[tuple[int,str,str]]]" = {}
            for hbond in line.split('\t'):
                don, acc = hbond.split('-')
                don, datom = don.split(':')
                acc, aatom = acc.split(':')
                don = int(don)
                acc = int(acc)
                if don not in data:
                    data[don] = []
                data[don].append((acc, datom, aatom))
            
            residues.update(data.keys())
            residues.update([a[0] for d in data.values() for a in d])
            hbonds.append(data)

    residues = np.array(sorted(residues), int)

    return hbonds, residues

def collapse(data : 'list[dict[int,list[tuple[int,str,str]]]]', residues : np.ndarray) -> np.ndarray:
    '''For every frame in the simulation, calculate the number of hbonds formed by every residue
    '''
    residues = np.array(residues)
    num_hbonds = np.zeros((len(data), len(residues)))

    for i, hbonds in enumerate(data):
        for don in hbonds:
            for (acc, datom, aatom) in hbonds[don]:
                num_hbonds[i, residues == don] += 1
                num_hbonds[i, residues == acc] += 1

    return num_hbonds


def _add_interaction(interactions, res1, res2, atom1, atom2):
    if res1 not in interactions:
        interactions[res1] = {}

    if res2 not in interactions[res1]:
        interactions[res1][res2] = {}

    atom_str = f'{atom1}-{atom2}'
    
    if atom_str not in interactions[res1][res2]:
        interactions[res1][res2][atom_str] = 0

    interactions[res1][res2][atom_str] += 1

def get_interactions(data : 'list[dict[int,list[tuple[int,str,str]]]]') -> 'dict[int, dict[int, float]]':
    '''For every residue, calculate the number of hbonds per frame formed with other residues.
    '''
    interactions = {}

    for hbonds in data:
        for don in hbonds:
            for (acc, datom, aatom) in hbonds[don]:
                _add_interaction(interactions, don, acc, datom, aatom)
                _add_interaction(interactions, acc, don, aatom, datom)

    for don in interactions:
        for acc in interactions[don]:
            for atom_str in interactions[don][acc]:
                interactions[don][acc][atom_str] /= len(data)

    return interactions

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='A program to further analyze the output of hbonds.py. Choose between different modes for different analysis.')

    parser.add_argument('mode', 
                        choices=['collapse', 'collapse_residues', 'interactions'], 
                        help='In which mode should the program run. Choose collapse to get the total number of hydrogen bonds per frame, collapse_residues for the total number of hydrogen bonds per residue and interactions to see a detailed list of interactions formed by the residues in question.')
    parser.add_argument('out_file', 
                        help='The path to the output file. Will override this if it already exists.')
    parser.add_argument('hbonds_files', 
                        nargs='+', 
                        help='The path to one or multiple hbond files, will assume that the order of the files is the order in a simulation.')
    parser.add_argument('--residues', 
                        default=None, 
                        help='Comma seperated list of residues to use.')

    args = parser.parse_args()

    hbonds = []
    all_residues = set()

    for file in args.hbonds_files:
        _hbonds, _residues = parse(file)

        hbonds.extend(_hbonds)
        all_residues.update(_residues)

    all_residues = np.array(sorted(all_residues))

    if args.residues:
        residues = np.array([int(res) for res in args.residues.split(',')])
    else:
        residues = all_residues

    res_bool = np.zeros(all_residues.shape, bool)
    for res in residues:
        res_bool = res_bool | (all_residues == res)

    if args.mode == 'collapse':
        data = collapse(hbonds, all_residues)
        data = data[:,res_bool]

        data = np.sum(data, 1)

        with open(args.out_file, 'w') as out_file:
            for x in data:
                out_file.write(str(x) + '\n')

    if args.mode == 'collapse_residues':
        data = collapse(hbonds, all_residues)
        data = data[:,res_bool]

        data = np.sum(data, 0) / data.shape[0]

        with open(args.out_file, 'w') as out_file:
            for x, residue in zip(data, all_residues[res_bool]):
                out_file.write(f'{residue},{x}\n')

    elif args.mode == 'interactions':
        data = get_interactions(hbonds)

        with open(args.out_file, 'w') as out_file:

            out_file.write(yaml.safe_dump(data, sort_keys=True))
