"""
Author: Georg Manthey (georg.manthey@uni-oldenburg.de)

This program will calculate the hydrogen bonds for a given protein pdb+psf file and corresponding dcd files. 

There can be multiple dcd files

For every frame in the dcd files, the script will output one line with all hydrogen bonds in the frame. 


"""

## Imports
import argparse
from itertools import repeat
import os
from prody import Selection
from prody.measure.measure import buildDistMatrix, calcAngle
from prody.proteins.pdbfile import parsePDB
from prody.trajectory.psffile import parsePSF
from prody.trajectory.trajectory import Trajectory
from multiprocessing import Pool, current_process
import numpy as np
import logging

logging.basicConfig(level=logging.DEBUG)

out_file = None
traj = None
global_frame = 0

def get_out_file(base_path):
    global out_file

    if not out_file:
        out_file = open(os.path.join(base_path, f'tmp_{current_process().pid}.txt'), 'a')    
        
    return out_file

def get_frame(args, framenum):
    global traj, global_frame

    if not traj:
        struct = parsePDB(args.pdb)
        struct = parsePSF(args.psf, ag=struct)

        traj = Trajectory(args.dcd[0])

        for dcd in args.dcd[1:]:
            traj.addFile(dcd)

        traj.link(struct)

        prot = struct.select(args.selection)

        traj.setAtoms(prot)

    while global_frame < framenum:
        next(traj)
        global_frame += 1

    global_frame += 1

    return next(traj)

def gather_indices(base_path):
    indices = set()

    for file in os.listdir(base_path):
        if not file.startswith('tmp'):
            continue

        with open(os.path.join(base_path, file), 'r') as f:
            for line in f:
                index = int(line.split(':')[0])
                indices.add(index)

    return indices


def gather_output(base_path):
    lines = []
    indices = []
    
    for file in os.listdir(base_path):
        if not file.startswith('tmp'):
            continue

        with open(os.path.join(base_path, file), 'r') as in_file:
            for line in in_file:
                index = int(line.split(':')[0])
                indices.append(index)
                lines.append(line)

    argsort = np.argsort(indices)
    lines = np.array(lines, str)[argsort]

    with open(os.path.join(base_path, 'hbonds.txt'), 'w') as out:
        out.write(''.join(lines))

def positive_numbers():
    x = 0
    while True:
        yield x
        x += 1

def frameloop(args):
    framenum, args = args
    #logging.debug(f'Calculating frame {framenum}')

    frame = get_frame(args, framenum)

    atoms : Selection = frame.getAtoms()

    ## Calculate distances between all atoms for cutoff values
    dist = buildDistMatrix(atoms)

    acceptors = []

    ## Iterate through all possible donors
    for i, don in enumerate(atoms):
        ## filter distance matrix for atoms closer that cutoff value
        close_atoms = np.argwhere(dist[i] <= args.cutoff)

        ## Iterate through all atoms sharing a bond with the donor
        ## This deals with a problem where some hydrogens were not called H in the name column
        for hyd in don.iterBonded():
            ## Filter out non-hydrogens
            if hyd.getName()[0] != 'H':
                continue

            ## Iterate through Atoms closer than the cutoff distance
            for acc in [atoms[j] for j in close_atoms if j != i]:
                ## Calculate angle and compare to cutoff angle
                if 180 - calcAngle(don, hyd, acc) <= args.angle:
                    ## Add Acceptor to Donors Acceptors
                    acceptors.append(f'{don.getResnum()}:{don.getName()}-{acc.getResnums()[0]}:{acc.getNames()[0]}')
    
    out = get_out_file(args.out_folder)

    out.write(f'{framenum}:' + '\t'.join(acceptors) + '\n') 
    out.flush()


# Print out acceptors
if __name__ == '__main__':
        ## Parse the arguments

    parser = argparse.ArgumentParser(description='Calculates hydrogen bonds from a trajectory.')

    parser.add_argument('pdb', 
                        help='A pdb file containing a protein (will be filtered for protein).')
    parser.add_argument('psf', 
                        help='A psf file describing the bonds of the pdb file.')
    parser.add_argument('out_folder', 
                        help='The folder to which the program should write intermediate and final output')
    parser.add_argument('dcd', 
                        nargs='+', 
                        help='One or more dcd files describing a trajectory of the pdb file.')
    parser.add_argument('--cutoff', '-c', 
                        default=3.0, 
                        type=float, 
                        help='The maximum distance between Donor and Acceptor in Angstrom.')
    parser.add_argument('--angle', '-a', 
                        default=20, 
                        type=int, 
                        help='The maximum distance from 180 degrees of the angle at the Hydrogen atom in degrees.')
    parser.add_argument('--maxFrame', '-m', 
                        default=0, 
                        type=int, 
                        help='Only calculate the hydrogen bonds up to this frame. Set to 0 for calculating all frames.')
    parser.add_argument('--chunk_size', 
                        default = 100, 
                        type=int, 
                        help='Chunksize for the parallelisation, if you have many frames, choose a higher number.')
    parser.add_argument('--selection', '-s', 
                        default='protein and not hydrogen', 
                        help='Atom selection text for the atoms considered as donators and acceptors.')
    parser.add_argument('--num_cpu', '-n', 
                        default=1, 
                        type=int, 
                        help='Number of cpu cores to use.')

    args = parser.parse_args()

    if not os.path.exists(args.out_folder):
        os.makedirs(args.out_folder)

    ## Read files

    struct = parsePDB(args.pdb)
    struct = parsePSF(args.psf, ag=struct)

    _traj = Trajectory(args.dcd[0])

    for dcd in args.dcd[1:]:
        _traj.addFile(dcd)

    _traj.link(struct)

    prot = struct.select(args.selection)

    _traj.setAtoms(prot)

    framenum = 0

    for _ in _traj:
        framenum += 1

    logging.debug(f'Calculating hbonds for {framenum} frames.')

    done_indices = gather_indices(args.out_folder)

    logging.debug(f'Skipping {len(done_indices)} of already calculated frames')

    calculate_list = [x for x in zip(range(framenum), repeat(args)) if (x[0] not in done_indices and (x[0] < args.maxFrame or args.maxFrame == 0))]

    logging.debug(f'This leaves us with {len(calculate_list)} frames to calculate')

    with Pool(args.num_cpu) as p:
        p.map(frameloop, calculate_list, args.chunk_size)

    logging.debug('Collecting output')
        
    gather_output(args.out_folder)